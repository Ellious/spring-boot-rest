package el.duisekeyev.repository;

import el.duisekeyev.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by elmarduisekeyev on 6/17/17.
 */

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
